<?php

/**
 * Реализация hook_permission
 */ 
function site_agree_permission() {
    
  return array(
	'administer siteagree' => array(
	  'title' => t('Administer SiteAgree settings')
	),
  );
  
}

/**
 * Реализация hook_menu
 */
function site_agree_menu() {
  
  $items = array();

  //настроечная форма - настройки валюты
  $items['adminmenu/siteagree/settings'] = array(
	'title' => 'Privacy', 
	'page callback' => 'drupal_get_form',
	'page arguments' => array('site_agree_setting_form'),
	'access arguments' => array('administer siteagree'),
	'file' => 'site_agree.admin.inc',
	'file path' => drupal_get_path('module', 'site_agree') . '/includes',  
	'type' => MENU_NORMAL_ITEM,
	'menu_name' => 'menu-adminmenu',
	'weight' => 2
  );
  // добавляем иконку
  pr_admin_f_api_adminmenu_icon_set('adminmenu/siteagree/settings', 'fa fa-check-circle');

  return $items;
}

/**
 * Реализация hook_menu_alter
 */
function site_agree_menu_link_alter(&$item) {
  
    //для пункта модуля
    if ((isset($item['path'])) and ($item['path'] == 'adminmenu/siteagree/settings'))  {
        pr_admin_f_api_submenu_create($item, 'adminmenu/users');
    }
    
}