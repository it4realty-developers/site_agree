/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function(){
  
  //если идет процесс отладки выходим
  if(jQuery('#block-site-agree-access-cookies').hasClass('debug')){
    return false;
  }
  //обрабатывае клик по кнопке в блоке доступ к cookies
  jQuery('#block-site-agree-access-cookies button').click(function() {
    //устанавливаем значение cookies в 1
    var myDate = new Date(); 
    myDate.setFullYear(myDate.getFullYear() + 1); 
    jQuery.cookie('access_cookie', '1', { expires: myDate });
    //скрываем блок
    jQuery('#block-site-agree-access-cookies').hide();
  });
  
});


