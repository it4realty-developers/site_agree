<?php

/**** Настройки валют ****/
function site_agree_setting_form($form, &$form_state) {
  
  $form['#attributes']['class'][] = 'pr_from';
  
  $form['site_agree_text_cookie_debug'] = array(
	'#type' =>'checkbox', 
	'#title' => t('Show always'),
	'#description' => t('Enable the user to see a message, even if press the "OK" (used for debugging)'),
	'#default_value' => variable_get('site_agree_text_cookie_debug', 0),  
  );
  
  // контейнер для языковый табов
  $form['section_langs']['#type'] = 'container';
  $form['section_langs']['#theme'] = 'pr_blocklist';
  $form['section_langs']['#show_type'] = 'tabs';
  $form['section_langs']['#weight'] = 0;
  $form['section_langs']['#class_tabs_container'][] = 'pr_form--langtab';
  
  //получем перечень языков на сайте для frontend
  $site_languages = pr_common_f_langlist_frontend();
  
  foreach ($site_languages as $langcode => $lang_value) {
    $form['section_langs']['tab_'.$langcode]['#type'] = 'container';
    $form['section_langs']['tab_'.$langcode]['#title_tab'] = $lang_value->name;
    $def_text_value = variable_get('site_agree_text_cookie_access_'.$langcode, '');
    $form['section_langs']['tab_'.$langcode]['site_agree_text_cookie_access_'.$langcode] = array(
	  '#type' => 'text_format',
	  '#title' => t('Text warning the use of cookis').' ('.$lang_value->native.')',
	  '#format'=>'full_html',
	  '#default_value' => (!empty($def_text_value['value']))?$def_text_value['value']:'',
	);
  }
  
  return system_settings_form($form);
  
}
